#!/bin/sh

LOG_FILE="$(ls -t /opt/eastpect/log/active/worker0_*.log | head -n 1)"

STATS="$(tail -n 1000 $LOG_FILE | grep "Stats Cloud" | tail -n 1 | cut -d " " -f4-)"

echo $STATS
