#!/bin/sh

# usage: ./install_update.sh <section> <version>
# usage: ./install_update.sh engine 0.5.8-beta8

if [ -z "$1" -o -z "$2" ]; then
    echo "Section and version must be defined!"
    echo "***ERROR***"
    exit 0
fi

if [ -f "/root/.svn-update-server" ]; then
    SVN_UPDATE_SERVER="$(cat /root/.svn-update-server)"
else
    SVN_UPDATE_SERVER="https://updates.sunnyvalley.io"
fi

VERSION="$2"
HOST_UUID="$(/sbin/sysctl kern.hostuuid | sed 's/ //g' | cut -d":" -f2)"
BACKUP_DIR="/opt/eastpect/backup"
ENGINE_UPDATE_URL="$SVN_UPDATE_SERVER/updates/engine/eastpect_opnsense_update_$VERSION.tar.gz"
ENGINE_UPDATE_URL_SHA256="$ENGINE_UPDATE_URL.sha256"
ENGINE_UPDATE_FILE="/tmp/eastpect_opnsense_update_$VERSION.tar.gz"
ENGINE_UPDATE_FILE_SHA256="$ENGINE_UPDATE_FILE.sha256"
ENGINE_BACKUP_FILE="$BACKUP_DIR/engine_$(date '+%Y%m%d_%H%M%S').tar.gz"
DB_UPDATE_URL="$SVN_UPDATE_SERVER/updates/eastdb/eastdb_opnsense_update_$VERSION.tar.gz"
DB_UPDATE_FILE="/tmp/eastdb_opnsense_update_$VERSION.tar.gz"
DB_UPDATE_DIR="/tmp/eastdb_opnsense_update_$VERSION"
DB_BACKUP_FILE="$BACKUP_DIR/eastdb_$(date '+%Y%m%d_%H%M%S').tar.gz"
PROGRESS_FILE="/tmp/sensei_download.progress"
PID_FILE="/tmp/sensei_download.pid"
POST_INSTALL_SCRIPT="/opt/eastpect/scripts/updater/opnsense/18.1/update.sh"
CURRENT_ENGINE_VERSION="$(/opt/eastpect/bin/eastpect -V | grep -i "release" | cut -d" " -f2)"

rm -f $ENGINE_UPDATE_FILE
rm -f $ENGINE_UPDATE_FILE_SHA256
rm -f $DB_UPDATE_FILE
rm -f $PROGRESS_FILE
rm -f $PID_FILE
rm -rf $DB_UPDATE_DIR

mkdir -p $BACKUP_DIR
mkdir -p $DB_UPDATE_DIR

: > /tmp/sensei_update.progress

install_engine_update() {
    echo "Downloading update file..."
    curl --max-time 600 $ENGINE_UPDATE_URL?host_uuid=$HOST_UUID -o $ENGINE_UPDATE_FILE 2> $PROGRESS_FILE 1> /dev/null &
    CURL_PID=$!
    echo $CURL_PID > $PID_FILE
    wait $CURL_PID

    if [ $? -eq 0 ]; then
        echo "Update file has been downloaded."
    else
        echo "Could not download update file! (Curl error code: $?)"
        echo "***ERROR***"
        exit 0
    fi

    echo "Downloading update sha256 check file..."
    curl --max-time 30 $ENGINE_UPDATE_URL_SHA256?host_uuid=$HOST_UUID -o $ENGINE_UPDATE_FILE_SHA256 > /dev/null 2>&1

    if [ $? -ne 0 ]; then
        echo "Could not download update sha256 file! (Curl error code: $?)"
        echo "***ERROR***"
        exit 0
    fi

    DOWNLOADED_SHA256="$(sha256 -q $ENGINE_UPDATE_FILE)"
    REMOTE_SHA256="$(cat $ENGINE_UPDATE_FILE_SHA256 | sed 's/ /_/g' | cut -d"_" -f1)"

    if [ "$DOWNLOADED_SHA256" != "$REMOTE_SHA256" ]; then
        echo "Downloaded update file corrupted or could not download sha256 check file from update server!"
        echo "***ERROR***"
        exit 0
    fi

    echo "Taking backup of current state..."
    tar --exclude='*log*' --exclude='*output*' --exclude='*backup*' --exclude='*crash_dumps*' -pczf $ENGINE_BACKUP_FILE -C /opt/ eastpect

    echo "Extracting update file..."
    tar -xzf $ENGINE_UPDATE_FILE -C /opt/

    if [ $? -ne 0 ]; then
        echo "Failed extracting update file! (Tar error code: $?)"
        echo "***ERROR***"
        exit 0
    fi

    echo "Running post install scripts..."
    if [ -f $POST_INSTALL_SCRIPT ]; then
        sh $POST_INSTALL_SCRIPT $CURRENT_ENGINE_VERSION $VERSION
    fi

    echo "Engine installation has been completed successfully."
    echo "***DONE***"
}

install_database_update() {
    echo "Downloading update file..."
    if curl --max-time 360 $DB_UPDATE_URL?host_uuid=$HOST_UUID -o $DB_UPDATE_FILE 2> $PROGRESS_FILE 1> /dev/null
    then
        echo "Update file has been downloaded."
    else
        echo "Could not download update file! (Curl error code: $?)"
        echo "***ERROR***"
        exit 0
    fi

    echo "Extracting update file..."
    tar -xzf $DB_UPDATE_FILE -C $DB_UPDATE_DIR

    if [ $? -ne 0 ]; then
        echo "Failed extracting update file! (Tar error code: $?)"
        echo "***ERROR***"
        exit 0
    fi

    echo "Taking backup of current databases..."
    tar -pczvf $DB_BACKUP_FILE -C /opt/eastpect/ db

    echo "Installing database updates..."
    rm -rf /opt/eastpect/db
    mv $DB_UPDATE_DIR /opt/eastpect/db

    echo "Database installation has been completed successfully."
    echo "***DONE***"
}

if [ "$1" == "engine" ]; then
    install_engine_update 2>&1
else
    install_database_update 2>&1
fi
