#!/usr/local/bin/python2.7
import subprocess
import requests
import json
import sys
import os

message_json = '/tmp/sensei_updates.json'

host_uuid = subprocess.check_output('/sbin/sysctl kern.hostuuid | sed "s/ //g" | cut -d":" -f2', shell=True).rstrip()

engine_version = subprocess.check_output('/opt/eastpect/bin/eastpect -V | grep -i "release" | cut -d" " -f2', shell=True).rstrip()

with open('/opt/eastpect/db/VERSION', 'r') as f:
    database_version = f.read().rstrip()

if os.path.isfile('/root/.svn-update-server'):
    with open('/root/.svn-update-server', 'r') as file:
        svn_update_server = file.read().rstrip()
else:
    svn_update_server = 'https://updates.sunnyvalley.io'

params = '?host_uuid=' + host_uuid + '&engine_version=' + engine_version + '&database_version=' + database_version

engine_version_uri = svn_update_server + '/updates/engine/latest.txt' + params
db_version_uri = svn_update_server + '/updates/eastdb/latest.txt' + params
engine_version_check_uri = svn_update_server + '/updates/engine/eastpect_opnsense_update_%s.tar.gz' + params
db_version_check_uri = svn_update_server + '/updates/eastdb/eastdb_opnsense_update_%s.tar.gz' + params

suffixes = ['devel', 'alpha1', 'alpha2', 'alpha3', 'alpha4', 'alpha5', 'alpha6', 'alpha7', 'alpha8', 'alpha9', 'alpha10',
            'beta1', 'beta2', 'beta3', 'beta4', 'beta5', 'beta6', 'beta7', 'beta8', 'beta9', 'beta10',
            'rc1', 'rc2', 'rc3', 'rc4', 'rc5', 'rc6', 'rc7', 'rc8', 'rc9', 'rc10', 'release']

updates = {
    'engine': {
        'update_required': False,
        'current_version': None,
        'latest_version': None,
        'versions_available': [],
        'versions_checked': []
    },
    'database': {
        'update_required': False,
        'current_version': None,
        'latest_version': None,
        'compatible_latest_version': None,
        'versions_checked': []
    }
}


def message(msg):
    with open(message_json, 'w') as message_file:
        json.dump(msg, message_file)
    print(json.dumps(msg))
    sys.exit(0)


class EngineVersion:
    def __init__(self, version):
        self.suffix = version.split('-')[1]
        self.major = int(version.split('-')[0].split('.')[0])
        self.minor = int(version.split('-')[0].split('.')[1])
        self.patch = int(version.split('-')[0].split('.')[2])
        self.generate_version_str()

    def generate_version_str(self):
        try:
            self.version = '%d.%d.%d-%s' % (self.major, self.minor, self.patch, self.suffix)
            self.version_str = '%d.%d.%d-%s' % (self.major, self.minor, self.patch, ('%02d' % (suffixes.index(self.suffix),)))
        except:
            message({'message': 'Non-standard suffix: %s' % self.suffix})

    def increment_suffix(self):
        i = suffixes.index(self.suffix) + 1
        i = i if i < len(suffixes) else 0
        self.suffix = suffixes[i]
        if i == 0:
            self.patch += 1
        self.generate_version_str()

    def check_exists(self):
        updates['engine']['versions_checked'].append(self.version)
        uri = engine_version_check_uri % self.version
        try:
            request = requests.head(uri)
        except:
            message({'message': 'Connection could not be established with the update server!'})
        exists = request.status_code < 400
        if exists:
            updates['engine']['versions_available'].append(self.version)
            self.increment_suffix()
        else:
            if self.suffix.startswith('alpha'):
                self.suffix = 'beta1'
            elif self.suffix.startswith('beta'):
                self.suffix = 'rc1'
            elif self.suffix.startswith('rc'):
                self.suffix = 'release'
            else:
                self.increment_suffix()
        if self.patch > 15:
            self.patch = 0
            self.minor += 1
        if self.minor > 10:
            self.minor = 0
            self.major += 1
        self.generate_version_str()
        return exists


try:
    engine_current = EngineVersion(engine_version)
except:
    message({'message': 'Non-standard engine version detected!'})

try:
    engine_latest = requests.get(engine_version_uri)
    engine_latest = EngineVersion(engine_latest.text.rstrip())
except:
    message({'message': 'Connection could not be established with the update server!'})

updates['engine']['current_version'] = engine_current.version
updates['engine']['latest_version'] = engine_latest.version

if engine_current.version_str < engine_latest.version_str:
    engine_current.increment_suffix()
    while engine_current.version_str <= engine_latest.version_str:
        engine_current.check_exists()

if len(updates['engine']['versions_available']) > 0:
    updates['engine']['update_required'] = True


class DatabaseVersion:
    def __init__(self, version):
        self.major = int(version.split('.')[0])
        self.minor = int(version.split('.')[1])
        self.patch = int(version.split('.')[2])
        self.generate_version()

    def generate_version(self):
        self.version = '%d.%d.%d' % (self.major, self.minor, self.patch)

    def check_exists(self):
        updates['database']['versions_checked'].append(self.version)
        uri = db_version_check_uri % self.version
        try:
            request = requests.head(uri)
        except:
            message({'message': 'Connection could not be established with the update server!'})
        exists = request.status_code < 400
        if exists:
            updates['database']['compatible_latest_version'] = self.version
        self.patch += 1
        self.generate_version()
        return exists


try:
    database_current = DatabaseVersion(database_version)
except:
    message({'message': 'Non-standard database version detected!'})

try:
    database_latest = requests.get(db_version_uri)
    database_latest = DatabaseVersion(database_latest.text.rstrip())
except:
    message({'message': 'Connection could not be established with the update server!'})

updates['database']['current_version'] = database_current.version
updates['database']['latest_version'] = database_latest.version

if database_latest.major == engine_current.major and database_latest.minor == engine_current.minor:
    updates['database']['compatible_latest_version'] = database_latest.version
else:
    compatible_version = DatabaseVersion('%d.%d.0' % (engine_current.major, engine_current.minor))
    exists = compatible_version.check_exists()
    while exists:
        exists = compatible_version.check_exists()

if updates['database']['compatible_latest_version'] > updates['database']['current_version']:
    updates['database']['update_required'] = True
else:
    updates['database']['compatible_latest_version'] = None
    updates['database']['update_required'] = False

message(updates)
