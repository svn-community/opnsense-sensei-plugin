#!/bin/sh

PROGRESS_FILE="/tmp/sensei_download.progress"

if [ -f "$PROGRESS_FILE" ]
then
    cat $PROGRESS_FILE | tr '\r' '\n' | tail -n 1
else
    echo "0"
fi
