#!/bin/sh

# usage: ./read_stats.sh <interface-index>
# usage: ./read_stats.sh 0

if [ -z "$*" ]
then
    echo "Interface index must be specified!"
    exit 0
fi

LOG_FILE="/opt/eastpect/log/active/worker$1_*.log"

if [ -z "$(ls -A $LOG_FILE 2> /dev/null)" ]
then
    echo "Could not find log file!"
    exit 0
fi

LOG_FILE="$(ls -t $LOG_FILE | head -n 1)"

LAN="$(tail -n 100 $LOG_FILE | grep "Stats LAN" | tail -n 1 | cut -d " " -f5)"
OCTETS="$(tail -n 100 $LOG_FILE | grep "Stats Octets" | tail -n 1)"
PACKETS="$(tail -n 100 $LOG_FILE | grep "Stats Packets" | tail -n 1)"

echo "interface:$LAN"

echo "bytes_out:$(echo $OCTETS | cut -d " " -f7)"
echo "bytes_in:$(echo $OCTETS | cut -d " " -f12 | sed 's/Octets://g')"

echo "tput_out:$(echo $OCTETS | cut -d " " -f9-10 | sed 's/]//g')"
echo "tput_in:$(echo $OCTETS | cut -d " " -f14-15 | sed 's/]//g')"

echo "packets_out:$(echo $PACKETS | cut -d " " -f6 | sed 's/pkt://g')"
echo "packets_in:$(echo $PACKETS | cut -d " " -f10 | sed 's/pkt://g')"

echo "errors_out:$(echo $PACKETS | cut -d " " -f7 | sed 's/drp://g')"
echo "errors_in:$(echo $PACKETS | cut -d " " -f11 | sed 's/drp://g')"

echo "pps_out:$(echo $PACKETS | cut -d " " -f8 | sed 's/pps://g' | sed 's/]//g')"
echo "pps_in:$(echo $PACKETS | cut -d " " -f12 | sed 's/pps://g' | sed 's/]//g')"
