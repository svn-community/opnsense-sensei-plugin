#!/bin/sh

UNINSTALL_LOG="/root/uninstall_sensei.log"

uninstall_sensei() {
    echo "Stopping elasticsearch service..."
    if service elasticsearch onestatus
    then
        service elasticsearch forcestop &
        sleep 7
        for PID in $(pgrep -u elasticsearch)
        do
            kill -9 $PID
        done
        sleep 3
    fi
    if service elasticsearch onestatus
    then
        echo "Could not stop elasticsearch service!"
        echo "Please stop elasticsearch service manually."
        echo "Exiting..."
        exit 0
    fi

    echo "Removing dependencies of Sensei..."
    pkg remove -y elasticsearch5 nagios-plugins
    pkg autoremove -y

    echo "Deleting elasticsearch store..."
    rm -rf /var/db/elasticsearch

    echo "Unmounting /dev/fd and /proc filesystems..."
    umount /dev/fd
    umount /proc

    echo "Cleaning elasticsearch rc.conf file..."
    sysrc -x elasticsearch_login_class

    echo "Reverting /etc/fstab (A backup will be created as /etc/fstab.bak)..."
    cp /etc/fstab /etc/fstab.bak
    sed -i '' -E '/fdescfs|procfs/d' /etc/fstab

    echo "Disabling FreeBSD repository (it is disabled by default!)..."
    sed -i '' 's/FreeBSD: { enabled: yes/FreeBSD: { enabled: no/g' /usr/local/etc/pkg/repos/FreeBSD.conf

    echo "Removing rc service script..."
    rm /usr/local/etc/rc.d/eastpect

    echo "Removing rc.conf.d files..."
    rm /etc/rc.conf.d/elasticsearch
    rm /etc/rc.conf.d/eastpect

    echo "Removing sensei files..."
    rm -rf /opt/eastpect

    echo "Removing cron jobs..."
    /usr/local/bin/php /usr/local/opnsense/mvc/app/models/OPNsense/Sensei/CLI.php crons remove

    echo "Removing web gui files..."
    find /usr/local/opnsense -iname "*sensei*" -type d -exec echo "Deleting directory: {}" \; -prune -exec rm -rf "{}" \;
    find /usr/local/opnsense -iname "*sensei*" -type f -exec echo "Deleting file: {}" \; -exec rm -f "{}" \;
    find /usr/local/opnsense -iname "*eastpect*" -type d -exec echo "Deleting directory: {}" \; -prune -exec rm -rf "{}" \;
    find /usr/local/opnsense -iname "*eastpect*" -type f -exec echo "Deleting file: {}" \; -exec rm -f "{}" \;

    rm -f /usr/local/opnsense/mvc/app/cache/*.php
    rm -f /tmp/opnsense_menu_cache.xml
    rm -f /tmp/configdmodelfield.data

    service configd restart
    echo "Sensei packet engine uninstalled succesfully."
    configctl webgui restart
}

if pgrep eastpect
then
    echo "Sensei packet engine is running. You must stop it firstly, in order to uninstall Sensei packet engine!"
else
    uninstall_sensei 2>&1 | tee $UNINSTALL_LOG
fi
exit 0
