<?php

require_once('script/load_phalcon.php');

use \OPNsense\Sensei\Sensei;
use \OPNsense\Core\Backend;
use \OPNsense\Cron\Cron;
use \OPNsense\Core\Config;

$sensei = new Sensei();
$backend = new Backend();
$cronMdl = new Cron();

if ($argv[1] == 'onboot') {
    if (!in_array($argv[2], ['enable', 'disable'])) {
        exit("You can only \"enable\" or \"disable\" this option!\n");
    } else {
        echo "Saving changes...\n";
        $sensei->getNodeByReference('onboot')->setNodes([
            'eastpect' => $argv[2] == 'enable' ? 'YES' : 'NO'
        ]);
        $sensei->saveChanges();
        echo "Re-generating configuration files...\n";
        $backend->configdRun('template reload OPNsense/Sensei');
        echo "You have ".$argv[2]."d auto-start of Sensei packet engine.\n";
    }
}

if ($argv[1] == 'crons') {
    if ($argv[2] != 'remove') {
        exit("You can only remove cron jobs for now!\n");
    } else {
        $cronNodes = $cronMdl->getNodeByReference('jobs.job')->getNodes();
        $deleted = 0;
        foreach ($cronNodes as $uuid=>$node) {
            if (
                $node['description'] == 'Sensei check health' or
                $node['description'] == 'Check Sensei packet engine health' or
                $node['description'] == 'Sensei check updates' or
                $node['description'] == 'Sensei datastore retire'
            ) {
                $cron = $cronMdl->jobs->job->del($uuid);
                echo "Cron job deleted: \"".$node['description']."\"\n";
                $deleted += 1;
            }
        }
        if ($deleted > 0) {
            $cronMdl->serializeToConfig();
            Config::getInstance()->save();
            $backend->configdRun('template reload OPNsense/Cron');
            $backend->configdRun('cron restart');
        }
    }
}

if ($argv[1] == 'migrate') {

    echo "Sensei database sync started.\n";

    if (file_exists($sensei->webControlsJson)) {
        $filters = file_get_contents($sensei->webControlsJson);
        $filters = json_decode($filters, true);

        $currentFilters = [];
        $filterNames = [];

        $deleteFilters = 0;
        $createdFilters = 0;
        $deletedCustomFilters = 0;

        foreach ($filters['categories'] as $category) {
            array_push($filterNames, $category['name']);
        }

        $nodes = $sensei->getNodeByReference('rules.webcategories')->getNodes();
        foreach ($nodes as $uuid=>$node) {
            if (!in_array($node['name'], $filterNames)) {
                $sensei->getNodeByReference('rules.webcategories')->del($uuid);
                $deleteFilters += 1;
            } else {
                array_push($currentFilters, $node['name']);
            }
        }

        foreach ($filters['categories'] as $category) {
            if (!in_array($category['name'], $currentFilters)) {
                array_push($currentFilters, $category['name']);
                $node = $sensei->rules->webcategories->Add();
                $node->setNodes([
                    'name' => $category['name']
                ]);
                $createdFilters += 1;
            }
        }

        $nodes = $sensei->getNodeByReference('rules.webcategories')->getNodes();
        foreach ($nodes as $uuid=>$node) {
            foreach ($filters['categories'] as $category) {
                if ($node['name'] == $category['name']) {
                    $sensei->getNodeByReference('rules.webcategories.'.$uuid)->setNodes([
                        'security' => $category['security']
                    ]);
                }
            }
        }

        $ruleNodes = $sensei->getNodeByReference('rules.customwebrules')->getNodes();
        foreach ($ruleNodes as $uuid=>$rule) {
            if (is_null($sensei->getNodeByReference('rules.customwebcategories.'.$rule['catid']))) {
                $sensei->getNodeByReference('rules.customwebrules')->del($uuid);
                $deletedCustomFilters += 1;
            }
        }

        echo "Web filters deleted: ".$deleteFilters."\n";
        echo "Web filters created: ".$createdFilters."\n";
        echo "Custom web filters deleted: ".$deletedCustomFilters."\n";
    } else {
        echo "Could not find ".$sensei->webControlsJson." file. Skipping synchronizing web controls list.\n";
    }

    if(file_exists($sensei->appControlsJson)) {
        $apps = file_get_contents($sensei->appControlsJson);
        $apps = json_decode($apps, true);

        $appNames = [];
        $appCatNames = [];

        $currentApps = [];
        $currentAppCats = [];

        $deletedApps = 0;
        $deletedAppCats = 0;
        $createdApps = 0;
        $createdAppCats = 0;
        $updatedApps = 0;

        foreach ($apps['apps'] as $app) {
            if (!in_array($app['name'], $appNames)) {
                array_push($appNames, $app['name']);
            }
            if (!in_array($app['category'], $appCatNames)) {
                array_push($appCatNames, $app['category']);
            }
        }

        $appNodes = $sensei->getNodeByReference('rules.apps')->getNodes();
        foreach ($appNodes as $uuid=>$node) {
            if (!in_array($node['name'], $appNames)) {
                $sensei->getNodeByReference('rules.apps')->del($uuid);
                $deletedApps += 1;
            } else {
                array_push($currentApps, $node['name']);
            }
        }

        $appCatNodes = $sensei->getNodeByReference('rules.appcategories')->getNodes();
        foreach ($appCatNodes as $uuid=>$node) {
            if (!in_array($node['name'], $appCatNames)) {
                $sensei->getNodeByReference('rules.appcategories')->del($uuid);
                $deletedAppCats += 1;
            } else {
                array_push($currentAppCats, $node['name']);
            }
        }

        foreach ($apps['apps'] as $app) {
            if (!in_array($app['name'], $currentApps)) {
                array_push($currentApps, $app['name']);
                $node = $sensei->rules->apps->Add();
                $node->setNodes([
                    'name' => $app['name'],
                    'description' => $app['description'],
                    'category' => $app['category'],
                    'web20' => $app['web20']
                ]);
                $createdApps += 1;
            }
            if (!in_array($app['category'], $currentAppCats)) {
                array_push($currentAppCats, $app['category']);
                $node = $sensei->rules->appcategories->Add();
                $node->setNodes([
                    'name' => $app['category']
                ]);
                $createdAppCats += 1;
            }
        }

        $appNodes = $sensei->getNodeByReference('rules.apps')->getNodes();
        foreach ($appNodes as $uuid=>$node) {
            foreach ($apps['apps'] as $app) {
                if ($app['name'] == $node['name'] and (
                    $app['description'] != $node['description'] or
                    $app['category'] != $node['category'] or
                    $app['web20'] != $node['web20']
                )) {
                    $sensei->getNodeByReference('rules.apps.'.$uuid)->setNodes([
                        'description' => $app['description'],
                        'category' => $app['category'],
                        'web20' => $app['web20']
                    ]);
                    $updatedApps += 1;
                }
            }
        }

        echo "Apps deleted: ".$deletedApps."\n";
        echo "Apps created: ".$createdApps."\n";
        echo "Apps updated: ".$updatedApps."\n";
        echo "App categories deleted: ".$deletedAppCats."\n";
        echo "App categories created: ".$createdAppCats."\n";
    } else {
        echo "Could not find ".$sensei->appControlsJson." file. Skipping synchronizing app controls list.\n";
    }

    $sensei->saveChanges();
    echo "Sensei database sync finished.\n";
}
