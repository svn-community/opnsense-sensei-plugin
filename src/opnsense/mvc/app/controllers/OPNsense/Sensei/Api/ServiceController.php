<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Core\Backend;
use \OPNsense\Core\Config;
use \OPNsense\Cron\Cron;
use \OPNsense\Sensei\Sensei;

class ServiceController extends ApiControllerBase
{
    private $cronDescription = 'Sensei check health';
    private $cronLegacyDescription = 'Check Sensei packet engine health';
    private $updateCronDescription = 'Sensei check updates';

    public function indexAction()
    {
        $sensei = new Sensei();
        $backend = new Backend();
        $response = [];
        $service = $this->request->getPost('service');
        $action = $this->request->getPost('action');
        $ifs = $sensei->getNodeByReference('interfaces')->getNodes();
        if ($service == 'eastpect' and ($action == 'start' or $action == 'restart') and empty($ifs)) {
            $response['message'] = 'You must select at least one interface to start or restart sensei service!';
            return $response;
        }
        $this->sessionClose();
        $response['output'] = $backend->configdRun('sensei service '.$service.' '.$action);
        return $response;
    }

    public function proxyAction()
    {
        $url = $this->request->getPost('url');
        $data = $this->request->getPost('data');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'http://localhost/'.$url);
        curl_setopt($curl, CURLOPT_PORT, 9200);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        $results = curl_exec($curl);
        if ($results === false || curl_getinfo($curl, CURLINFO_HTTP_CODE) >= 400) {
            $this->response->setStatusCode(503, 'Service Unavailable');
        } else {
            $this->response->setHeader('Content-Type', 'application/json; charset=utf-8');
        }
        curl_close($curl);
        return $results;
    }

    public function cronAction()
    {
        $cronMdl = new Cron();
        $response = [];
        $cronNodes = $cronMdl->getNodeByReference('jobs.job')->getNodes();
        $this->deleteLegacyCron($cronMdl);
        if ($this->request->getMethod() == 'GET') {
            foreach ($cronNodes as $uuid=>$node) {
                if ($node['description'] == $this->cronDescription) {
                    $response['exists'] = true;
                    return $response;
                }
            }
            $response['exists'] = false;
            return $response;
        } elseif ($this->request->getMethod() == 'POST') {
            $isChanged = false;
            if ($this->request->getPost('enable') == 'true') {
                foreach ($cronNodes as $uuid=>$node) {
                    if ($node['description'] == $this->cronDescription) {
                        $response['message'] = 'Cron job already exists';
                        return $response;
                    }
                }
                $cron = $cronMdl->jobs->job->Add();
                $cron->setNodes([
                    'origin' => 'Sensei',
                    'command' => 'sensei check-health',
                    'description' => $this->cronDescription,
                    'minutes' => '*',
                    'hours' => '*'
                ]);
                $isChanged = true;
                $response['message'] = 'Cron job has been created successfully';
            } else {
                foreach ($cronNodes as $uuid=>$node) {
                    if ($node['description'] == $this->cronDescription) {
                        $cron = $cronMdl->jobs->job->del($uuid);
                        $isChanged = true;
                    }
                }
                $response['message'] = 'Cron job has been deleted successfully';
            }
            if ($isChanged) {
                $this->saveCronChanges($cronMdl);
            }
            return $response;
        }
    }

    public function setUpdateCron($enabled=false)
    {
        $cronMdl = new Cron();
        $cronNodes = $cronMdl->getNodeByReference('jobs.job')->getNodes();
        $isChanged = false;
        if ($enabled) {
            foreach ($cronNodes as $uuid=>$node) {
                if ($node['description'] == $this->updateCronDescription) {
                    return true;
                }
            }
            $cron = $cronMdl->jobs->job->Add();
            $cron->setNodes([
                'origin' => 'Sensei',
                'command' => 'sensei check-updates',
                'description' => $this->updateCronDescription,
                'minutes' => '0',
                'hours' => '*'
            ]);
            $isChanged = true;
        } else {
            foreach ($cronNodes as $uuid=>$node) {
                if ($node['description'] == $this->updateCronDescription) {
                    $cron = $cronMdl->jobs->job->del($uuid);
                    $isChanged = true;
                }
            }
        }
        if ($isChanged) {
            $this->saveCronChanges($cronMdl);
        }
    }

    private function saveCronChanges($cronMdl)
    {
        $backend = new Backend();
        $cronMdl->serializeToConfig();
        Config::getInstance()->save();
        $backend->configdRun('template reload OPNsense/Cron');
        $backend->configdRun('cron restart');
    }

    private function deleteLegacyCron($cronMdl)
    {
        $isChanged = false;
        $cronNodes = $cronMdl->getNodeByReference('jobs.job')->getNodes();
        foreach ($cronNodes as $uuid=>$node) {
            if ($node['description'] == $this->cronLegacyDescription) {
                $cron = $cronMdl->jobs->job->del($uuid);
                $isChanged = true;
            }
        }
        if ($isChanged) {
            $this->saveCronChanges($cronMdl);
        }
    }
}
