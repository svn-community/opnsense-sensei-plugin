<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Core\Backend;
use \OPNsense\Sensei\Sensei;

class EngineController extends ApiControllerBase
{
    public function statusAction()
    {
        $backend = new Backend();
        $sensei = new Sensei();
        $onBoots = $sensei->getNodeByReference('onboot')->getNodes();
        return [
            'eastpect' => [
                'status' => strpos($backend->configdRun('sensei service eastpect status'), 'is running') !== false,
                'onboot' => $onBoots['eastpect'] == 'YES',
                'engine' => [
                    'version' => preg_replace('/\R+/', '', $backend->configdRun('sensei engine-version')),
                    'lastUpdate' => preg_replace('/\R+/', '', $backend->configdRun('sensei engine-date'))
                ],
                'db' => [
                    'version' => preg_replace('/\R+/', '', $backend->configdRun('sensei db-version')),
                    'lastUpdate' => preg_replace('/\R+/', '', $backend->configdRun('sensei db-date'))
                ],
                'rules' => [
                    'version' => preg_replace('/\R+/', '', $backend->configdRun('sensei db-version')),
                    'lastUpdate' => preg_replace('/\R+/', '', $backend->configdRun('sensei db-date'))
                ]
            ],
            'elasticsearch' => [
                'status' => strpos($backend->configdRun('sensei service elasticsearch status'), 'is running') !== false,
                'onboot' => $onBoots['elasticsearch'] == 'YES'
            ]
        ];
    }

    public function statsAction()
    {
        $backend = new Backend();
        $sensei = new Sensei();
        $interfaces = $sensei->getNodeByReference('interfaces')->getNodes();
        $results = [];
        $index = 0;
        foreach ($interfaces as $interface) {
            $stats = $backend->configdRun('sensei stats-read '.$index);
            $obj = [];
            foreach (explode("\n", $stats) as $stat) {
                if ($stat) {
                    $stat = explode(':', $stat);
                    if (count($stat) > 1) {
                        $obj[$stat[0]] = $stat[1];
                    }
                }
            }
            array_push($results, $obj);
            $index++;
        }
        return $results;
    }

    public function reloadAction($section=null)
    {
        $sensei = new Sensei();
        $this->sessionClose();
        return $sensei->reloadEngine($section);
    }

    public function cloudNodesAction()
    {
        $backend = new Backend();
        $response = [];
        $stats = $backend->configdRun('sensei cloud-stats-read');
        $stats = trim(preg_replace('/\s+/', ' ', $stats));
        if ($stats) {
            $stats = explode('|', explode(']', explode('[', $stats)[1])[0]);
            foreach ($stats as $stat) {
                $cols = explode(' ', trim($stat));
                if (count($cols) > 4) {
                    $details = array_pop($cols);
                    $success = array_pop($cols);
                    $response_time = array_pop($cols);
                    $status = array_pop($cols);
                    array_push($response, [
                        'node' => join(' ', $cols),
                        'status' => $status,
                        'response' => $response_time,
                        'success' => $success,
                        'details' => $details
                    ]);
                }
            }
        }
        return $response;
    }

    public function cloudNodeRecordsAction()
    {
        $sensei = new Sensei();
        $response = [];
        if (file_exists($sensei->cloudReputationServersFile)) {
            foreach (explode("\n", file_get_contents($sensei->cloudReputationServersFile)) as $item) {
                if ($item) {
                    $recs = explode(',', $item);
                    if (count($recs) > 2) {
                        array_push($response, [
                            'servername' => $recs[0],
                            'ip' => $recs[1],
                            'port' => $recs[2]
                        ]);
                    }
                }
            }
        }
        return $response;
    }
}
