<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Core\Backend;
use \OPNsense\Sensei\Sensei;

class UpdateController extends ApiControllerBase
{
    public function checkAction($section=null)
    {
        $sensei = new Sensei();
        $this->sessionClose();
        if ($section) {
            if ($section == 'engine') {
                return $sensei->checkUpdates($auto=false, $section='engine');
            } elseif ($section == 'database') {
                return $sensei->checkUpdates($auto=false, $section='database');
            } elseif ($section == 'auto') {
                return $sensei->checkUpdates();
            }
        } else {
            return $sensei->checkUpdates($auto=false);
        }
    }

    public function installAction($section=null, $version=null)
    {
        $sensei = new Sensei();
        $backend = new Backend();
        $response = [];
        $response['msg_uuid'] = $backend->configdRun('sensei update-install '.$section.' '.$version, true);
        if (file_exists($sensei->updatesJson)) {
            unlink($sensei->updatesJson);
        }
        return $response;
    }

    public function statusAction()
    {
        $backend = new Backend();
        $response = [];
        $response['outputs'] = $backend->configdRun('sensei update-status');
        return $response;
    }

    public function progressAction()
    {
        $backend = new Backend();
        $response = [];
        $response['progress'] = intval(explode(' ', trim($backend->configdRun('sensei read-download-progress')))[0]);
        return $response;
    }

    public function cancelAction()
    {
        $backend = new Backend();
        $response = [];
        $response['output'] = $backend->configdRun('sensei update-cancel');
        $response['successful'] = stripos($response['output'], 'canceled') !== false;
        return $response;
    }

    public function uninstallAction()
    {
        $sensei = new Sensei();
        $response = [];
        $response['outputs'] = shell_exec($sensei->uninstallScript);
        return $response;
    }

    public function postAction()
    {
        $sensei = new Sensei();
        if (!file_exists($sensei->configDoneFile)) {
            touch($sensei->configDoneFile);
        }
        return 'OK';
    }
}
