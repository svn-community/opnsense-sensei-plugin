<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Core\Config;
use \OPNsense\IDS\IDS;

class ToolsController extends ApiControllerBase
{
    public function interfacesAction()
    {
        $response = [];
        $config = Config::getInstance()->object();
        if ($config->interfaces->count() > 0) {
            foreach ($config->interfaces->children() as $key=>$node) {
                $desc = strtoupper(!empty((string) $node->descr) ? (string) $node->descr : $key);
                $interface = (string) $node->if;
                $unique = [];
                if ($desc != 'WAN' and substr($interface, 0, 5) != 'vtnet' and !in_array($interface, $unique) and $node->virtual != '1' and !array_key_exists('wireless', $node)) {
                    array_push($unique, $interface);
                    array_push($response, [
                        'interface' => $interface,
                        'description' => $desc
                    ]);
                }
            }
        }
        return $response;
    }

    public function configAction()
    {
        return (array) Config::getInstance()->object();
    }

    public function executeAction() {
        $commands = $this->request->getPost('commands');
        $response = [];
        foreach ($commands as $command) {
            array_push($response, shell_exec($command));
        }
        return $response;
    }

    public function suricataInterfacesAction()
    {
        $idsMdl = new IDS();
        $response = [];
        if ((string) $idsMdl->general->enabled == 1) {
            foreach ($idsMdl->getNodeByReference('general')->getNodes()['interfaces'] as $key=>$value) {
                if ($value['selected'] == 1) {
                    array_push($response, $value['value']);
                }
            }
        }
        return $response;
    }
}
