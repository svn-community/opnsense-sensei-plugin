<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Core\Config;
use \OPNsense\Core\Backend;
use \OPNsense\Sensei\Sensei;
use \OPNsense\Sensei\Api\ServiceController;

class SettingsController extends ApiControllerBase
{
    public function indexAction()
    {
        $sensei = new Sensei();
        $serviceController = new ServiceController();
        if ($this->request->getMethod() == 'GET') {
            return [
                'netflow' => $sensei->getNodeByReference('netflow')->getNodes(),
                'enrich' => $sensei->getNodeByReference('enrich')->getNodes(),
                'tls' => $sensei->getNodeByReference('tls')->getNodes(),
                'dns' => $sensei->getNodeByReference('dns')->getNodes(),
                'general' => $sensei->getNodeByReference('general')->getNodes(),
                'updater' => $sensei->getNodeByReference('updater')->getNodes()
            ];
        } elseif ($this->request->getMethod() == 'POST') {
            $sensei->setNodes($this->request->getPost('config'));
            if ($this->request->hasPost('interfaces')) {
                $this->configureInterfaces($sensei, $this->request->getPost('interfaces'));
            }
            $this->generateTcpPasswordSha256($sensei);
            $sensei->saveChanges();
            $serviceController->setUpdateCron((string) $sensei->getNodeByReference('updater.autocheck') == 'true');
            return 'OK';
        }
    }

    public function allAction()
    {
        $sensei = new Sensei();
        return $sensei->getNodes();
    }

    public function writeAction()
    {
        $backend = new Backend();
        return $backend->configdRun('template reload OPNsense/Sensei');
    }

    private function configureInterfaces($sensei, $interfaces)
    {
        $backend = new Backend();
        $deleteInterfaces = [];
        $currentInterfaces = [];
        $interfaces = explode(',', $interfaces);
        $interfaceNodes = $sensei->getNodeByReference('interfaces')->getNodes();
        foreach ($interfaceNodes as $uuid=>$node) {
            if (in_array($node['name'], $interfaces)) {
                array_push($currentInterfaces, $node['name']);
            } else {
                array_push($deleteInterfaces, $uuid);
            }
        }
        foreach ($deleteInterfaces as $uuid) {
            $sensei->getNodeByReference('interfaces')->del($uuid);
        }
        foreach ($interfaces as $interface) {
            if ($interface and !in_array($interface, $currentInterfaces)) {
                $node = $sensei->interfaces->Add();
                $node->setNodes([
                    'name' => $interface
                ]);
            }
        }
        $cpuCount = intval(trim($backend->configdRun('sensei cpu-count'), "\n"));
        $interfaceNodes = $sensei->getNodeByReference('interfaces')->getNodes();
        $index = 0;
        foreach ($interfaceNodes as $uuid=>$node) {
            $sensei->getNodeByReference('interfaces.'.$uuid)->setNodes([
                'manageport' => (string) (4343 + $index),
                'cpuindex' => $cpuCount > 1 ? (string) (($index % ($cpuCount - 1)) + 1) : '0'
            ]);
            $index++;
        }
    }

    public function interfacesAction()
    {
        $interfaces = [];
        $sensei = new Sensei();
        $interfaceNodes = $sensei->getNodeByReference('interfaces')->getNodes();
        foreach ($interfaceNodes as $uuid=>$node) {
            array_push($interfaces, [
                'interface' => $node['name']
            ]);
        }
        return $interfaces;
    }

    public function landingPageAction()
    {
        $sensei = new Sensei();
        $landingPage = $sensei->landingPage;
        if ($this->request->getMethod() == 'GET') {
            if (file_exists($landingPage)) {
                return file_get_contents($landingPage);
            } else {
                return '<html><head><title>Not Found!</title></head></html>';
            }
        } else {
            $landingPage = $sensei->landingPage;
            $landingPageDir = dirname($landingPage);
            if (!file_exists($landingPageDir)) {
                mkdir($landingPageDir);
            }
            move_uploaded_file($_FILES['file']['tmp_name'], $landingPage);
            return 'OK';
        }
    }

    private function generateTcpPasswordSha256($sensei)
    {
        $tcpServicePsk = (string) $sensei->getNodeByReference('enrich.tcpServicePsk');
        $tcpServicePskSha256 = hash('sha256', $tcpServicePsk);
        $sensei->setNodes([
            'enrich' => [
                'tcpServicePskSha256' => $tcpServicePskSha256
            ]
        ]);
    }

    public function certsAction()
    {
        if ($this->request->getMethod() == 'GET') {
            $certs = [];
            $config = (array) Config::getInstance()->object();
            if (array_key_exists('ca', $config)) {
                if (array_key_exists('descr', $config['ca'])) {
                    $cert = (array) $config['ca'];
                    if (!empty($cert['prv'])) {
                        array_push($certs, [
                            'id' => 0,
                            'name' => $cert['descr']
                        ]);
                    }
                } else {
                    foreach ($config['ca'] as $key=>$node) {
                        $node = (array) $node;
                        if (!empty($node['prv'])) {
                            array_push($certs, [
                                'id' => $key,
                                'name' => $node['descr']
                            ]);
                        }
                    }
                }
            }
            return $certs;
        } elseif ($this->request->getMethod() == 'POST') {
            $sensei = new Sensei();
            $certFile = $sensei->certFile;
            $keyFile = $sensei->certKeyFile;
            $certPost = $this->request->getPost('cert');
            $keyPost = $this->request->getPost('prvt');
            if ($certPost && $keyPost) {
                file_put_contents($certFile, $certPost);
                file_put_contents($keyFile, $keyPost);
            }
            return 'OK';
        }
    }

    public function tlsWhiteListAction()
    {
        $sensei = new Sensei();
        $response = [];
        if ($this->request->getMethod() == 'GET') {
            $response['enabled'] = file_exists($sensei->tlsWhitelistFile);
        } elseif ($this->request->getMethod() == 'POST') {
            $enabled = $this->request->getPost('enabled');
            $defaultFile = $sensei->tlsWhitelistFileDefault;
            $rulesFile = $sensei->tlsWhitelistFile;
            if ($enabled == 'true') {
                if (!file_exists($rulesFile) && file_exists($defaultFile)) {
                    symlink($defaultFile, $rulesFile);
                }
            } else {
                if (file_exists($rulesFile)) {
                    unlink($rulesFile);
                }
            }
            $response['enabled'] = file_exists($sensei->tlsWhitelistFile);
        }
        return $response;
    }

    public function reportsAction()
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'GET') {
            return $sensei->getNodeByReference('reports')->getNodes();
        } elseif ($this->request->getMethod() == 'POST') {
            $config = $this->request->getPost('config');
            $sensei->getNodeByReference('reports')->setNodes($config);
            $sensei->saveChanges();
            return 'OK';
        }
    }

    public function resetAction()
    {
        $sensei = new Sensei();
        $serviceController = new ServiceController();
        $response = [];
        foreach (['apps', 'appcategories', 'webcategories'] as $rule) {
            $count = 0;
            $nodes = $sensei->getNodeByReference('rules.'.$rule)->getNodes();
            foreach ($nodes as $key=>$value) {
                $sensei->getNodeByReference('rules.'.$rule.'.'.$key)->setNodes([
                    'action' => 'accept',
                    'writetofile' => 'no'
                ]);
                $count++;
            }
            array_push($response, $count.' '.$rule.' restored to defaults');
        }
        foreach (['customwebcategories', 'customwebrules'] as $rule) {
            $count = 0;
            $nodes = $sensei->getNodeByReference('rules.'.$rule)->getNodes();
            foreach ($nodes as $key=>$value) {
                $sensei->getNodeByReference('rules.'.$rule)->del($key);
                $count++;
            }
            array_push($response, $count.' '.$rule.' deleted');
        }
        $interfaces = $sensei->getNodeByReference('interfaces')->getNodes();
        foreach ($interfaces as $key=>$value) {
            $sensei->getNodeByReference('interfaces')->del($key);
        }
        array_push($response, 'Protected interfaces deleted');
        $serviceController->setUpdateCron(false);
        array_push($response, 'Update cron job deleted');
        $sensei->setNodes([
            'general' => [
                'coreFileEnable' => 'false',
                'flavor' => 'small'
            ],
            'netflow' => [
                'enabled' => 'false',
                'version' => '9',
                'collectorip' => '127.0.0.1',
                'collectorport' => '9996'
            ],
            'updater' => [
                'enabled' => 'true',
                'autocheck' => 'true',
                'lastupdate' => ''
            ],
            'onboot' => [
                'eastpect' => 'YES',
                'elasticsearch' => 'YES'
            ],
            'reports' => [
                'refresh' => '60000',
                'interval' => '604800000',
                'custominterval' => [
                    'start' => '',
                    'end' => ''
                ],
                'sum' => 'sessions'
            ],
            'dns' => [
                'localDomain' => 'intra.example.com'
            ],
            'tls' => [
                'enabled' => 'false',
                'certname' => '',
                'passtopsites' => 'false'
            ],
            'enrich' => [
                'tcpServiceEnable' => 'true',
                'tcpServiceIP' => '127.0.0.1',
                'tcpServicePsk' => 'Sensei1234',
                'tcpServicePskSha256' => '',
                'cloudWebcatEnrich' => 'false'
            ]
        ]);
        array_push($response, 'All settings were reset to default values');
        $sensei->saveChanges();
        if (file_exists($sensei->webControlsJson.'.done')) {
            rename($sensei->webControlsJson.'.done', $sensei->webControlsJson);
        }
        if (file_exists($sensei->appControlsJson.'.done')) {
            rename($sensei->appControlsJson.'.done', $sensei->appControlsJson);
        }
        if (file_exists($sensei->configDoneFile)) {
            unlink($sensei->configDoneFile);
        }
        array_push($response, 'Finished');
        return $response;
    }
}
