<?php
namespace OPNsense\Sensei;

use \OPNsense\Sensei\Sensei;

class IndexController extends \OPNsense\Base\IndexController
{
    public function indexAction()
    {
        $sensei = new Sensei();
        $this->view->wizardRequired = file_exists($sensei->configDoneFile) ? 'false' : 'true';
        $this->response->setHeader("Content-Security-Policy", "default-src * 'unsafe-inline' 'unsafe-eval'; script-src * 'unsafe-inline' 'unsafe-eval'; connect-src * 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src *; style-src * 'unsafe-inline';");
        $this->view->pick('OPNsense/Sensei/index');
    }
}
