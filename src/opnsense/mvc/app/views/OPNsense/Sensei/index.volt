<!-- OPNsense Libraries -->
<script src="/ui/js/moment-with-locales.min.js"></script>
<script src="/ui/js/d3.min.js"></script>

<!-- Sensei Libraries -->
<script src="/ui/js/d3.layout.cloud.min.js"></script>
<script src="/ui/js/bootstrap-datetimepicker.min.js"></script>
<script src="/ui/js/jquery.bootstrap-growl.min.js"></script>

<!-- Sensei Application -->
<div aurelia-app="main">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="progress">
                <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:50%" id="init-progress"></div>
            </div>
            <h2 class="text-center text-primary" id="init-text">Initializing...</h2>
        </div>
    </div>
    <script>
        $('#Sensei>a.active').removeClass('active');
        var wizardRequired = {{wizardRequired}};
    </script>
    <script type="text/javascript" src="/ui/js/sensei-app.js?v=1537041763"></script>
    <script type="text/javascript" src="/ui/js/sensei-vendor.js?v=1537041763"></script>
</div>
